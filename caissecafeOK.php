<?php
declare (strict_types=1);
/*
Pour simplifier l’exercice, on le découpe en petite partie 

Saisir le nombre de café “A”, 
Saisir le nombre de soda “B”, 
Saisir le nombre de bière “C”, 
Calculer le montant de la note = (A * 1.10€) + (B*2.20€) + (C * 2.40€) 
Afficher le “Total à payer” 
On va créer un tableau pour stocker les prix afin que le cafetier puisse ensuite utiliser son programme même s’il a changé ses prix. */

function Saisi_Valeur($message){
    (int) $x = -1;
    while ($x <= 0):
        print ($message);
        $x = intval(trim(fgets(STDIN)));
    endwhile;
    return $x;
}
    


// ----------------------Tableau des prix --------------------

    ( array ) $prixConso =[
        "Cafe" => ["prixHT" => 1.00, "tva" => 0.1],
        "Soda" => ["prixHT" => 2.00, "tva" => 0.1],
        "Biere" => ["prixHT" => 2.00, "tva" => 0.2]
    ];

    ( array ) $caisseFinale = [
        "nombreCafeSaisis" => 0,
        "nombreSodaSaisis" => 0,
        "nombreBiereSaisis" => 0,
        "montantVenteHT" => 0.0,
        "montantTVA" => 0.0,
        "montantVenteTTC" => 0.0
    ];


    //Initialisation de la variable pour faire tourner la boucle des saisies des ventes
    ( bool ) $saisieTicket = true;

// ----------------------Entrée des données ventes ----------------
while ($saisieTicket):

    // Juste pour le plaisir de donner la date ------------------------
    $jour = date('d');
    $mois = date ('m');
    $annee= date ('Y');
    $heure = date ('H');
    $minute = date ('i');
    print ('Nous sommes le ' .$jour . '/' 
    . $mois . '/' 
    . $annee . ' et il est ' 
    . $heure. 'h'
    . $minute. '.' .PHP_EOL);

    // ----------------------Variable ---------------------------
    ( int ) $nombreCafe = 0;
    ( float ) $montantCafeHT = 0.0;
    ( int ) $nombreSoda = 0;
    ( float ) $montantSodaHT = 0.0;
    ( int ) $nombreBiere = 0;
    ( float ) $montantBiereHT = 0.0;
    ( float ) $montantHT = 0.0;
    ( float ) $montantTTC = 0.0 ;
    ( float ) $montantDonne = 0.0;
    ( float ) $monnaieARendre = 0.0;



    
    $nombreCafe = Saisi_Valeur ("Veuillez entrer le nombre de cafe consommés :" .PHP_EOL);
    $caisseFinale["nombreCafeSaisis"] = $nombreCafe + $caisseFinale["nombreCafeSaisis"];

    $nombreSoda = Saisi_Valeur ("Veuillez entrer le nombre de soda consommés :" .PHP_EOL);
    $caisseFinale["nombreSodaSaisis"] = $nombreSoda + $caisseFinale["nombreSodaSaisis"];

    $nombreBiere = Saisi_Valeur ("Veuillez entrer le nombre de biére consommés :" .PHP_EOL);
    $caisseFinale["nombreBiereSaisis"] = $nombreBiere + $caisseFinale["nombreBiereSaisis"];

    // -------------------Calcul des prix ---------------------------   
    //prix du cafe et de sa TVA 
    $montantCafeHT = $nombreCafe*$prixConso["Cafe"]["prixHT"];
    $montantCafeTVA = $montantCafeHT * $prixConso["Cafe"]["tva"];

    //prix des sodas et de sa TVA
    $montantSodaHT = $nombreSoda*$prixConso["Soda"]["prixHT"];
    $montantSodaTVA = $montantSodaHT * $prixConso["Soda"]["tva"];
    
    //prix des bières et de sa TVA
    $montantBiereHT = $nombreBiere*$prixConso["Biere"]["prixHT"];
    $montantBiereTVA = $montantBiereHT * $prixConso["Biere"]["tva"];
    

    // ---------------------Impression du ticket ----------------------
    print ('' .PHP_EOL);
    print ('TICKET'.PHP_EOL);
    printf ('%d café à 1€ = %d €'.PHP_EOL , $nombreCafe, $montantCafeHT);
    printf ('%d soda à 2€ = %d €'.PHP_EOL , $nombreSoda, $montantSodaHT);
    printf ('%d bière à 2€ = %d €'.PHP_EOL , $nombreBiere, $montantBiereHT);
    
    
    //Calcul des différentes recettes
    $montantHT = $montantCafeHT + $montantSodaHT + $montantBiereHT;
    $montantTVA = $montantCafeTVA + $montantSodaTVA + $montantBiereTVA;
    $montantTTC = $montantHT + $montantTVA;
    
    //enregistrement des données dans le tableau
    $caisseFinale["montantHT"] = $montantHT;
    $caisseFinale["montantTVA"] = $montantTVA;
    $caisseFinale["montantTTC"] = $montantTTC;
    $caisseFinale["montantVenteHT"] = $montantHT + $caisseFinale["montantVenteHT"];
    $caisseFinale["montantTVA"] = $montantTVA + $caisseFinale["montantTVA"];
    $caisseFinale["montantVenteTTC"] = $montantTTC + $caisseFinale["montantVenteTTC"];

    printf ('Le montant HT est de %.2d €.'.PHP_EOL, $montantHT);
    printf ('La TVA est de %.2f €.' .PHP_EOL, $montantTVA);
    printf ('Le montant TTC est de %.2f €.' .PHP_EOL, $montantTTC);

    print ('Entrez la somme donnée par le client :' .PHP_EOL);
    $montantDonne = floatval(trim(fgets(STDIN)));
    $monnaieARendre = $montantDonne - $montantTTC;
    printf ('Le montant à rendre est de %.2f €.' .PHP_EOL, $monnaieARendre);

    //fin de saisie : choix "on continue" "on arrete la saisie"
    ( string ) $choixFin = "";

    while ($choixFin != 'o' && $choixFin != 'n') :
        print ('Avez-vous un autre achat à saisir ? o/n' .PHP_EOL);
        $choixFin = (trim(strval(fgets(STDIN))));

        if ($choixFin == 'o') :
            print ('Retour à la saisie' .PHP_EOL);
 
        //impression de la caisse de fin de journée
        else :
            print ('' .PHP_EOL);
            print ('//////////////////////////////////////////' .PHP_EOL);
            print ('La caisse va être close pour aujourd\'hui.'.PHP_EOL);
            print ('Voici le récapitulatif des ventes de la journées :' .PHP_EOL);
            printf ('Le nombre de cafés vendus est : %d.'.PHP_EOL, $caisseFinale["nombreCafeSaisis"]);
            printf ('Le nombre de sodas vendus est de : %d.' .PHP_EOL, $caisseFinale["nombreSodaSaisis"]);
            printf ('Le nombre de bières vendues est de : %d.' .PHP_EOL, $caisseFinale["nombreBiereSaisis"]);
            printf ('Le montant total des recettes HT est de : %.2f €.' .PHP_EOL, $caisseFinale["montantVenteHT"]);
            printf ('Le montant de TVA à reverser est de : %.2f €.' .PHP_EOL, $caisseFinale["montantTVA"]);
            printf ('Le montant des recettes s\'élève à %.2f €.' .PHP_EOL, $caisseFinale["montantVenteTTC"]);
        endif;

    endwhile;

    $saisieTicket = ($choixFin == 'o');

endwhile;
    

/* dernière instruction = faire une boucle pour enregistrer les achats au fur
et à mesure afin de pouvoir faire une caisse à la fin et donc prevoir un affichage en
fin de journée avec les recettes HT et TTC, la tva à reverser et le nombre de 
consommation de chaque */ 